Initial configuration:
 
0 0 0 0 8 3 0 0 6 0 7 0 
0 4 0 7 0 0 0 0 5 2 0 12 
9 0 0 11 0 5 7 0 0 0 10 4 
11 7 0 2 0 10 0 0 0 0 0 5 
0 0 12 0 0 0 0 0 0 7 0 0 
0 0 0 0 12 0 0 5 0 11 0 0 
0 0 3 0 9 0 0 1 0 0 0 0 
0 0 4 0 0 0 0 0 0 12 0 0 
6 0 0 0 0 0 5 0 3 0 8 2 
12 2 0 0 0 9 6 0 4 0 0 3 
1 0 8 4 0 0 0 0 2 0 12 0 
0 9 0 3 0 0 12 2 0 0 0 0 

Number of filled squares: 50

After completing: 

10 12 5 1 8 3 2 4 6 9 7 11 
8 4 6 7 10 1 9 11 5 2 3 12 
9 3 2 11 6 5 7 12 1 8 10 4 
11 7 9 2 4 10 8 6 12 3 1 5 
3 5 12 6 11 2 1 9 8 7 4 10 
4 1 10 8 12 7 3 5 9 11 2 6 
2 10 3 12 9 8 4 1 11 6 5 7 
7 8 4 5 2 6 11 3 10 12 9 1 
6 11 1 9 7 12 5 10 3 4 8 2 
12 2 7 10 5 9 6 8 4 1 11 3 
1 6 8 4 3 11 10 7 2 5 12 9 
5 9 11 3 1 4 12 2 7 10 6 8 

1. Backtracking

No. of times backtracked: 12030730
Time taken to solve (in milliseconds): 156447

2. Backtracking + MRV

No. of times backtracked: 45889
Time taken to solve (in milliseconds): 791

3. Backtracking + MRV + forward checking

No. of times backtracked: 39933
Time taken to solve (in milliseconds): 1277

4. Backtracking + MRV + Constraint propagation

No. of times backtracked: 11982
Time taken to solve (in milliseconds): 660
