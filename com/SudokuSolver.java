package com;


public class SudokuSolver extends AbstractSudokuSolver {
	
	public SudokuSolver(String inputFileName){
		//init();
		inputFile = inputFileName;
		readInput();
	}
	
	public boolean backtrackAndSolve(){
		recursionDepth++;
		if (isStuck()){
			return false;
		}
		for (int x = 0; x < N; x++){
			for (int y = 0; y < N; y++){
				if (board[x][y] == 0){
					for (int i = 0; i < N; i++){
						if (allowed[x][y][i]){
							int[][] tempBoard = new int[N][N];//board.clone();
							boolean[][][] tempAllowed = new boolean[N][N][N];
							getBoardCopy(tempBoard, board);
							//setValue(i+1, x, y);
							//int[][] tempBoard = board.clone();
							//boolean[][][] tempAllowed = allowed.clone();
							getAllowedCopy(tempAllowed, allowed);
							setValue(i+1, x, y);
							if (backtrackAndSolve()){
								return true;
							}
							getBoardCopy(board, tempBoard);
							getAllowedCopy(allowed, tempAllowed);
							//board = tempBoard;
							//allowed = tempAllowed;
						}
					}
					return false;
				}//  if data == 0
			} // for y
		} // for x
		return isBoardFilled();
		//return false;
	}
	
	public void solve(){
		if(!backtrackAndSolve()){
			System.out.println("Wrong board configuration. No solution exists.");
		}
		//System.out.println("recursion depth: " + recursionDepth);
	}
	
	public static void main(String args[]){
		if(args.length != 1){
			System.out.println("Input file not specified as argument\n");
			return;
		}
			
		SudokuSolver solver = new SudokuSolver(args[0]);
		System.out.println("Initial configuration:\n ");
		solver.printBoard();
		System.out.println("Number of filled squares: " + solver.getNumOfFilledSquares());
		System.out.println("\nSolving sudoku...\n");
		long startTime = System.currentTimeMillis();
		solver.solve();
		long endTime = System.currentTimeMillis();
		System.out.println("After completing: \n");
		solver.printBoard();
		System.out.println("No. of times backtracked: " + solver.getRecursionDepth());
		System.out.println("Time taken to solve (in milliseconds): " + (endTime - startTime));
	}
}
