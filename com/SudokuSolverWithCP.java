package com;

import java.util.ArrayList;

public class SudokuSolverWithCP extends AbstractSudokuSolver {
	
	public SudokuSolverWithCP(String inputFileName){
		//init();
		inputFile = inputFileName;
		readInput();
	}
	
	public boolean solveWithMRVAndCP(){
		recursionDepth++;
		if (isStuck()){
			return false;
		}
		
		int[] nextBoxToFill = getNextBoxToFill();
		int x = nextBoxToFill[0];
		int y = nextBoxToFill[1];
		if(x != -1 && y != -1 ){
			for (int i = 0; i < N; i++){
				if (allowed[x][y][i] && canAssignWithCP(x, y, i+1)){
					int[][] tempBoard = new int[N][N];//board.clone();
					boolean[][][] tempAllowed = new boolean[N][N][N];
					getBoardCopy(tempBoard, board);
					getAllowedCopy(tempAllowed, allowed);
					setValue(i+1, x, y);
					if (solveWithMRVAndCP()){
						return true;
					}
					getBoardCopy(board, tempBoard);
					getAllowedCopy(allowed, tempAllowed);
				}
			}
			return false;
		}
		return isBoardFilled();
	}
	
	public void solve(){
		//if(!solveWithMRVAndFC()){
		//if(!backtrackAndSolveWithMRV()) {
		if(!solveWithMRVAndCP()){
			System.out.println("No solution..");
		}
		//System.out.println("recursion depth: " + recursionDepth);
	}
	
	private int[] getNextBoxToFill(){
		int[] pos = new int[2];
		int minX = -1, minY = -1;
		int minAllowedVals = N+1, noOfAllowedVals;
		
		for(int i=0;i<N;i++){
			for(int j=0;j<N;j++){
				if(board[i][j] == 0){
					noOfAllowedVals = 0;
					for(int k=0;k<N;k++){
						if(allowed[i][j][k])
							noOfAllowedVals++;
					}
					if(noOfAllowedVals < minAllowedVals){
						minAllowedVals = noOfAllowedVals;
						minX = i;
						minY = j;
					}
				}
			}
		}
		
		pos[0] = minX;
		pos[1] = minY;
		return pos;
	}
	
	private ArrayList<Integer> getAllowedValues(boolean[][][] tempAllowed, int x, int y){
		ArrayList<Integer> arr = new ArrayList<Integer>();
		for(int i=0;i<N;i++){
			if(tempAllowed[x][y][i])
				arr.add(i+1);
		}
		return arr;
	}
	
	private boolean canAssignWithCP(int x, int y, int value){
		boolean[][][] tempAllowed = new boolean[N][N][N];
		getAllowedCopy(tempAllowed, allowed);
		return assign(tempAllowed, x, y, value);
	
	}
	
	private boolean assign(boolean[][][] tempAllowed, int x, int y, int value){
		//tempAllowed[x][y][value-1] = false;
		for(int i=0;i<N;i++){
			if(i != (value - 1) && tempAllowed[x][y][i] && !eliminate(tempAllowed, x, y, i+1))
					return false;
		}		
		return true;
	}
	
	private boolean eliminate(boolean[][][] tempAllowed, int x, int y, int valueToEliminate){
		int i,j;
		
		if(!tempAllowed[x][y][valueToEliminate-1])
			return true; //already eliminated

		tempAllowed[x][y][valueToEliminate-1] = false;
		
		ArrayList<Integer> allowedValues = getAllowedValues(tempAllowed, x, y);
		
		if(allowedValues.size() == 0)
			return false; //removed last value
		
		if(allowedValues.size() == 1){
			//check if we can eliminate that 1 allowed value from peers of x,y
			//if not return false
			
			int newValueToEliminate = allowedValues.get(0);
			
			//row & col peers
			for(i = 0; i < N; i++){
				if(i!=y && !eliminate(tempAllowed, x, i, newValueToEliminate))
					return false;
				if(i!=x && !eliminate(tempAllowed, i, y, newValueToEliminate))
					return false;
			}
			
			//sub-square peers
			int x_orig = M * (int) (x/M);
			int y_orig = K * (int) (y/K);
			for (i = x_orig; i < x_orig + M; i++){
				for (j = y_orig; j < y_orig + K; j++){
					if(i!=x && j!=y && !eliminate(tempAllowed, i, j, newValueToEliminate))
						return false;
				}
			}			
		}
		
		/*
		//If a unit has only one possible place for value, then put the value there.
		
		//check if there is only one place in row to put the value
		int yVal = -1, hitCount = 0;
		for(i=0;i<N;i++){
			if(tempAllowed[x][i][valueToEliminate-1]){
				yVal = i;
				hitCount++;
			}
		}
		if(hitCount == 0 || (hitCount == 1 && !assign(tempAllowed, x, yVal, valueToEliminate)))
			return false;
		
		//check if there is only one place in col to put the value
		int xVal = -1;
		for(i=0, hitCount = 0;i<N;i++){
			if(tempAllowed[i][y][valueToEliminate-1]){
				xVal = i;
				hitCount++;
			}
		}
		if(hitCount == 0 || (hitCount == 1 && !assign(tempAllowed, x, xVal, valueToEliminate)))
			return false;
		
		
		//check if there is only one place in sub-square to put the value
		int x_orig = K * (int) (x/K);
		int y_orig = M * (int) (y/M);
		for (i = x_orig, hitCount = 0; i < x_orig + K; i++){
			for (j = y_orig; j < y_orig + M; j++){
				if(tempAllowed[i][j][valueToEliminate - 1]){
					hitCount++;
					xVal = i;yVal = j;
				}
			}
		}
		if(hitCount == 0 || (hitCount == 1 && !assign(tempAllowed, xVal, yVal, valueToEliminate)))
			return false;
		*/
		return true;
	}
	
	
	
	public static void main(String args[]){
		if(args.length != 1){
			System.out.println("Input file not specified as argument\n");
			return;
		}
		
		SudokuSolverWithCP solver = new SudokuSolverWithCP(args[0]);
		System.out.println("Initial configuration:\n ");
		solver.printBoard();
		System.out.println("Number of filled squares: " + solver.getNumOfFilledSquares());
		System.out.println("\nSolving sudoku...\n");
		long startTime = System.currentTimeMillis();
		solver.solve();
		long endTime = System.currentTimeMillis();
		System.out.println("After completing: \n");
		solver.printBoard();
		System.out.println("No. of times backtracked: " + solver.getRecursionDepth());
		System.out.println("Time taken to solve (in milliseconds): " + (endTime - startTime));
	}
}
