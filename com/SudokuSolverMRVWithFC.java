package com;

public class SudokuSolverMRVWithFC extends AbstractSudokuSolver {
	
	public SudokuSolverMRVWithFC(String inputFileName){
		//init();
		inputFile = inputFileName;
		readInput();
	}
	
	public boolean solveWithMRVAndFC(){
		recursionDepth++;
		if (isStuck()){
			return false;
		}
		
		int[] nextBoxToFill = getNextBoxToFill();
		int x = nextBoxToFill[0];
		int y = nextBoxToFill[1];
		if(x != -1 && y != -1 ){
			for (int i = 0; i < N; i++){
				if (allowed[x][y][i] && forwardCheck(x, y, i+1)){
					int[][] tempBoard = new int[N][N];//board.clone();
					boolean[][][] tempAllowed = new boolean[N][N][N];
					getBoardCopy(tempBoard, board);
					getAllowedCopy(tempAllowed, allowed);
					setValue(i+1, x, y);
					if (solveWithMRVAndFC()){
						return true;
					}
					getBoardCopy(board, tempBoard);
					getAllowedCopy(allowed, tempAllowed);
				}
			}
			return false;
		}
		return isBoardFilled();
	}
	
	public void solve(){
		if(!solveWithMRVAndFC()){
		//if(!backtrackAndSolveWithMRV()) {
			System.out.println("No solution..");
		}
		//System.out.println("recursion depth: " + recursionDepth);
	}
	
	private int[] getNextBoxToFill(){
		int[] pos = new int[2];
		int minX = -1, minY = -1;
		int minAllowedVals = N+1, noOfAllowedVals;
		
		for(int i=0;i<N;i++){
			for(int j=0;j<N;j++){
				if(board[i][j] == 0){
					noOfAllowedVals = 0;
					for(int k=0;k<N;k++){
						if(allowed[i][j][k])
							noOfAllowedVals++;
					}
					if(noOfAllowedVals < minAllowedVals){
						minAllowedVals = noOfAllowedVals;
						minX = i;
						minY = j;
					}
				}
			}
		}
		
		pos[0] = minX;
		pos[1] = minY;
		return pos;
	}
	
	private boolean noMoreValuesAllowedForCol(boolean[][][] tempAllowed, int x, int y){
		int count;
		for(int i=0;i<N;i++){
		   if(i!= x){
			    count = 0;
				for(int k=0;k<N;k++){
					if(!tempAllowed[i][y][k]){
						count++;
					}
			    }
				if(count == N)
					return true;
		   }
		}
		return false;
	}
	
	private boolean noMoreValuesAllowedForRow(boolean[][][] tempAllowed, int x, int y){
		int count;
		for(int j=0;j<N;j++){
		   if(j!=y){
			    count = 0;
				for(int k=0;k<N;k++){
					if(!tempAllowed[x][j][k]){
						count++;
					}
			    }
				if(count == N)
					return true;
		   }
		}
		return false;
	}
	
	private boolean noMoreValuesAllowedForBox(boolean[][][] tempAllowed, int x, int y){
		int count =0;
		for(int k=0;k<N;k++){
			if(!tempAllowed[x][y][k])
				count++;
		}
		return (count == N);
	}
	
	private boolean noMoreValuesAllowedForSubSquare(boolean[][][] tempAllowed, int x, int y){
		int count;
		// now take care of MxK block
		int x_orig = M * (int) (x/M);
		int y_orig = K * (int) (y/K);
		for (int i = x_orig; i < x_orig + M; i++){
			for (int j = y_orig; j < y_orig + K; j++){
				if( i!= x && j != y && noMoreValuesAllowedForBox(tempAllowed, i, j))
					return true;
			}
		}
		return false;
	}
	
	private boolean forwardCheck(int x, int y, int value){
		boolean[][][] tempAllowed = new boolean[N][N][N];
		getAllowedCopy(tempAllowed, allowed);
		
		// set allowed[][][value-1] = false in rows and cols
		for(int i = 0; i < N; i++){
			tempAllowed[x][i][value - 1] = false;
			tempAllowed[i][y][value - 1] = false;
		}
			
		if(noMoreValuesAllowedForRow(tempAllowed, x, y) || noMoreValuesAllowedForCol(tempAllowed, x, y))
			return false;
		
		// now take care of MxK block
		int x_orig = M * (int) (x/M);
		int y_orig = K * (int) (y/K);
		for (int i = x_orig; i < x_orig + M; i++){
			for (int j = y_orig; j < y_orig + K; j++){
				tempAllowed[i][j][value - 1] = false;
			}
		}
		
		//no values are allowed anymore
		//for (int i = 0; i < N; i++)
		//	tempAllowed[x][y][i] = false;
		
		//tempAllowed[x][y][value - 1] = true;
		
		if(noMoreValuesAllowedForSubSquare(tempAllowed, x, y))
			return false;
		
		return true;
	}
	
	public static void main(String args[]){
		if(args.length != 1){
			System.out.println("Input file not specified as argument\n");
			return;
		}
		
		SudokuSolverMRVWithFC solver = new SudokuSolverMRVWithFC(args[0]);
		System.out.println("Initial configuration:\n ");
		solver.printBoard();
		System.out.println("Number of filled squares: " + solver.getNumOfFilledSquares());
		System.out.println("\nSolving sudoku...\n");
		long startTime = System.currentTimeMillis();
		solver.solve();
		long endTime = System.currentTimeMillis();
		System.out.println("After completing: \n");
		solver.printBoard();
		System.out.println("No. of times backtracked: " + solver.getRecursionDepth());
		System.out.println("Time taken to solve (in milliseconds): " + (endTime - startTime));
		
	}
}
