package com;

public class SudokuSolverMRV extends AbstractSudokuSolver {

	public SudokuSolverMRV(String inputFileName){
		//init();
		inputFile = inputFileName;
		readInput();
	}
	
	public boolean backtrackAndSolve(){
		recursionDepth++;
		if (isStuck()){
			return false;
		}
		for (int x = 0; x < N; x++){
			for (int y = 0; y < N; y++){
				if (board[x][y] == 0){
					for (int i = 0; i < N; i++){
						if (allowed[x][y][i]){
							int[][] tempBoard = new int[N][N];//board.clone();
							boolean[][][] tempAllowed = new boolean[N][N][N];
							getBoardCopy(tempBoard, board);
							//setValue(i+1, x, y);
							//int[][] tempBoard = board.clone();
							//boolean[][][] tempAllowed = allowed.clone();
							getAllowedCopy(tempAllowed, allowed);
							setValue(i+1, x, y);
							if (backtrackAndSolve()){
								return true;
							}
							getBoardCopy(board, tempBoard);
							getAllowedCopy(allowed, tempAllowed);
							//board = tempBoard;
							//allowed = tempAllowed;
						}
					}
					return false;
				}//  if data == 0
			} // for y
		} // for x
		return isBoardFilled();
		//return false;
	}
	
	public boolean backtrackAndSolveWithMRV(){
		recursionDepth++;
		if (isStuck()){
			return false;
		}
		
		int[] nextBoxToFill = getNextBoxToFill();
		int x = nextBoxToFill[0];
		int y = nextBoxToFill[1];
		if(x != -1 && y != -1 ){
			for (int i = 0; i < N; i++){
				if (allowed[x][y][i]){
					int[][] tempBoard = new int[N][N];//board.clone();
					boolean[][][] tempAllowed = new boolean[N][N][N];
					getBoardCopy(tempBoard, board);
					getAllowedCopy(tempAllowed, allowed);
					setValue(i+1, x, y);
					if (backtrackAndSolveWithMRV()){
						return true;
					}
					getBoardCopy(board, tempBoard);
					getAllowedCopy(allowed, tempAllowed);
				}
			}
			return false;
		}
		return isBoardFilled();
	}
	
	public void printBoard(){
		for(int i=0;i<N;i++){
			for(int j=0;j<N;j++){
				System.out.print(board[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
	
	public void randomGenerate(int numOfValues){
		int x, y, value;
		boolean isSet;
		
		for(int i=0;i<numOfValues;i++){
			isSet = false;
			while(!isSet){
				x = (int)(Math.random()*(double)N);
				y = (int)(Math.random()*(double)N);
				
				if(board[x][y] != 0)
					continue;
				
				value = (int)(Math.random()*(double)N) + 1;
				while(!isSetAllowed(value, x, y)){
					value = (int)(Math.random()*(double)N) + 1;
				}
				
				setValue(value, x, y);
				isSet = true;
			}
		}
	}
	
	public void solve(){
		if(!backtrackAndSolveWithMRV()){
			System.out.println("No solution..");
		}
		//System.out.println("recursion depth: " + recursionDepth);
	}
	
	private int[] getNextBoxToFill(){
		int[] pos = new int[2];
		int minX = -1, minY = -1;
		int minAllowedVals = N+1, noOfAllowedVals;
		
		for(int i=0;i<N;i++){
			for(int j=0;j<N;j++){
				if(board[i][j] == 0){
					noOfAllowedVals = 0;
					for(int k=0;k<N;k++){
						if(allowed[i][j][k])
							noOfAllowedVals++;
					}
					if(noOfAllowedVals < minAllowedVals){
						minAllowedVals = noOfAllowedVals;
						minX = i;
						minY = j;
					}
				}
			}
		}
		
		pos[0] = minX;
		pos[1] = minY;
		return pos;
	}
	
	public static void main(String args[]){
		if(args.length != 1){
			System.out.println("Input file not specified as argument\n");
			return;
		}
		
		SudokuSolverMRV solver = new SudokuSolverMRV(args[0]);
		System.out.println("Initial configuration:\n ");
		solver.printBoard();
		System.out.println("Number of filled squares: " + solver.getNumOfFilledSquares());
		System.out.println("\nSolving sudoku...\n");
		long startTime = System.currentTimeMillis();
		solver.solve();
		long endTime = System.currentTimeMillis();
		System.out.println("After completing: \n");
		solver.printBoard();
		System.out.println("No. of times backtracked: " + solver.getRecursionDepth());
		System.out.println("Time taken to solve (in milliseconds): " + (endTime - startTime));
	}
}
