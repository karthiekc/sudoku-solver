package com;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Scanner;

public abstract class AbstractSudokuSolver {

	int M, K, N;
	int[][] board;// = new int[N][N];
	boolean[][][] allowed;// = new boolean[N][N][N];
	String inputFile;
	long recursionDepth = 0;
	int numOfValuesSet = 0;
	
	public void readInput(){
		try {
			InputStream in = new FileInputStream(
					new File(inputFile));
			Scanner scanner = new Scanner(in);
			N = scanner.nextInt();
			M = scanner.nextInt();
			K = scanner.nextInt();
			
			board = new int[N][N];
			allowed = new boolean[N][N][N];
			
			init();
			
			String input = scanner.nextLine();
			while(input.isEmpty())
				input = scanner.nextLine();
			
			String[] arr = input.split(" ");
			int index = 0;
			for(int i=0;i<N;i++){
				for(int j=0;j<N;j++){
					//board[i][j] = arr[index++] - (int)'0';
					int value = Integer.parseInt(arr[index++]);
					if(value != 0) {
					  setValue(value, i, j);
					  numOfValuesSet++;
					}
				}
			}
			//System.out.println("Number of filled squares: " + numOfValuesSet);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int getNumOfFilledSquares(){
		return numOfValuesSet;
	}

	public void setValue(int value, int x, int y){
		if(board[x][y] != 0 || value<1 || value > N || !allowed[x][y][value - 1])
			return;
		
		board[x][y] = value;
		
		// set allowed[][][value-1] = false in rows and cols
		for(int i = 0; i < N; i++){
			allowed[x][i][value - 1] = false;
			allowed[i][y][value - 1] = false;
		}
		
		// now take care of MxK block
		int x_orig = M * (int) (x/M);
		int y_orig = K * (int) (y/K);
		for (int i = x_orig; i < x_orig + M; i++){
			for (int j = y_orig; j < y_orig + K; j++){
				allowed[i][j][value - 1] = false;
			}
		}
		
		//no values are allowed anymore
		for (int i = 0; i < N; i++)
			allowed[x][y][i] = false;
		
		allowed[x][y][value - 1] = true;
	}
	
	public boolean isSetAllowed(int value, int x, int y){
		return allowed[x][y][value - 1];
	}
	
	public long getRecursionDepth(){
		return recursionDepth;
	}
	
	public boolean isStuck(){
		for (int x = 0; x < N; x++){
			for (int y = 0; y < N; y++){
				if (board[x][y] == 0){
					boolean c = false;
					for (int i = 0; i < N; i++){
						c |= allowed[x][y][i];
					}
					if (!c){
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public void printBoard(){
		for(int i=0;i<N;i++){
			for(int j=0;j<N;j++){
				System.out.print(board[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
	
	public void randomGenerate(int numOfValues){
		int x, y, value;
		boolean isSet;
		
		for(int i=0;i<numOfValues;i++){
			isSet = false;
			while(!isSet){
				x = (int)(Math.random()*(double)N);
				y = (int)(Math.random()*(double)N);
				
				if(board[x][y] != 0)
					continue;
				
				value = (int)(Math.random()*(double)N) + 1;
				while(!isSetAllowed(value, x, y)){
					value = (int)(Math.random()*(double)N) + 1;
				}
				
				setValue(value, x, y);
				isSet = true;
			}
		}
	}
	
	protected void init(){
		for (int i = 0; i < N; i++){
			for (int j = 0; j < N; j++){
				board[i][j] = 0;
				for (int k = 0; k < N; k++){
					allowed[i][j][k] = true;
				}
			}
		}
	}
		
	public boolean isBoardFilled(){
		for(int i=0;i<N;i++){
			for(int j=0;j<N;j++){
				if(board[i][j] == 0)
					return false;
			}
		}
		return true;
	}
	
	public abstract void solve();
	
	protected void getBoardCopy(int[][] newBoard, int[][] oldBoard){
		for(int i=0;i<N;i++){
			for(int j=0;j<N;j++){
				newBoard[i][j] = oldBoard[i][j];
			}
		}
	}
	
	protected void getAllowedCopy(boolean[][][] newBoard, boolean[][][] oldBoard){
		for(int i=0;i<N;i++){
			for(int j=0;j<N;j++){
				for(int k=0;k<N;k++){
				  newBoard[i][j][k] = oldBoard[i][j][k];
			    }
		    }
		}
	}
}
